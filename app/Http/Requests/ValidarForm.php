<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidarForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required|max:255',
            'email' => 'required|unique:funcionarios',
            'cpf' => 'required|unique:funcionarios',
        ];
    }


    public function messages()
{
    return [
        'nome.required' => ' Nome Não Informado',
        'email.required' => 'E-Mail Não Informado',
        'email.unique' => 'E-Mail Já Cadastrado',
        'cpf.required' => 'CPF Não Informado',
        'cpf.unique' => 'CPF Já Cadastrado',

       
    ];

    }
}
