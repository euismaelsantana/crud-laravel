<?php

namespace App\Http\Controllers;

use App\Http\Requests\ValidarForm;
use Illuminate\Http\Request;
use App\Models\Funcionario as ModelsFuncionarios;

class Funcionario extends Controller
{
    private $funcionario;

    public function __construct(){  
        $this->funcionario = new ModelsFuncionarios();
    }

    /**
     * Listagem de funcionários.
     *
     * @return \Illuminate\Http\Response
     */
    public function listar()
    {
        $funcionario = $this->funcionario->all();
        return view('index', compact('funcionario'));
    }

    /**
     * View Cadastro.
     *
     * @return \Illuminate\Http\Response
     */
    public function novo()
    { 
        return view('cadastro');
    }

    /**
     * Criar novo funcionário.
     * 
     * @param \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function criar(ValidarForm $request)
    {
        $this->funcionario->create($request->all());
        return redirect('/');
    }

    /**
     * View Editar.
     *
     * @return \Illuminate\Http\Response
     */
    public function editar($id) 
    {
        $funcionario = $this->funcionario::findOrFail($id);
        return view('editar', ['funcionario' => $funcionario]);
    }

    /**
     * Alterar funcionário.
     *  
     * @param \Illuminate\Http\Request $request
     * @param string $id
     * @return \Illuminate\Http\Response
     */
    public function alterar(Request  $request, $id)
    {
        $this->funcionario::findOrFail( $id )->update($request->all());
        return redirect('/');
    }

    /**
     * Excluir funcionário.
     *
     * @param string $id
     * @return \Illuminate\Http\Response
     */
    public function excluir($id)
    {
        $this->funcionario::findOrFail( $id )->delete();
        return redirect('/');
    }
}
