<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
        <title>Projeto CRUD - Home</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <header class="jumbotron ">
                        <div class="container">
                            <h1 class="display-6 text-center">Funcionários</h1>
                            <p class="lead text-center">Nossos Funcionários Ativos.</p>
                        </div>
                    </header>
                    <div class="btn-novo-usuario">
                        <a href="{{url('cadastro')}}">
                            <button type="button" class="btn btn-primary btn-sm  ">NOVO</button>
                        </a>
                    </div>
                    <table class="table table-striped table-dark">
                        <thead>
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Nome</th>
                                <th scope="col">Email</th>
                                <th scope="col">Telefone</th>
                                <th scope="col">Cpf</th>
                                <th scope="col">Cargo</th>
                                <th colspan="2">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($funcionario as $funcionarios)
                                <tr>
                                    <th scope="row">{{$funcionarios->id}}</th>
                                    <td>{{$funcionarios->nome}}</td>
                                    <td>{{$funcionarios->email}}</td>
                                    <td>{{$funcionarios->telefone}}</td>
                                    <td>{{$funcionarios->cpf}}</td>
                                    <td>{{$funcionarios->cargo}}</td>
                                    <td>
                                        <a href="{{url('funcionario')}}/{{$funcionarios->id}}" class="btn btn-warning btn-sm acoes">Editar</a>
                                        <form class="acoes" action="funcionario/{{ $funcionarios->id }}" method="post">
                                            @csrf
                                            @method('delete')
                                            <button class="btn btn-danger btn-sm">Excluir</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </body>
</html>