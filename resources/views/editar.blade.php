<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
        <title>Projeto CRUD - Cadastro</title>
    </head>
    <body>
        <div class="container fundo-form">
            <div class="row">
                <div class="col-md-12">
                    <header class="jumbotron ">
                        <div class="container">
                            <h1 class="display-6 text-center">Funcionários</h1>
                            <p class="lead text-center">Editar Funcionários.</p>
                        </div>
                    </header>
                    <form method="post" action="{{url('funcionario')}}/{{$funcionario->id}}} ">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <label for="">Nome</label>
                            <input type="text" class="form-control" id=" " name="nome" value="{{$funcionario->nome}}">
                        </div>
                        <div class="form-group">
                            <label for="">Email</label>
                            <input type="email" value="{{$funcionario->email}}" class="form-control" id=" " name="email" aria-describedby="emailHelp">
                        </div>
                        <div class="form-group">
                            <label for="">Telefone</label>
                            <input type="tel" value="{{$funcionario->telefone}}" class="form-control" id=" " name="telefone" aria-describedby="emailHelp">
                        </div>
                        <div class="form-group">
                            <label for="">Cpf</label>
                            <input type="text" value="{{$funcionario->cpf}}" class="form-control" id=" " name="cpf" aria-describedby="emailHelp">
                        </div>
                        <div class="form-group">
                            <label for="">Cargo</label>
                            <input type="text" value="{{$funcionario->cargo}}" class="form-control" id="" name="cargo">
                        </div>
                        <button type="submit" class="btn btn-primary">Editar</button>
                        <a href="{{url('/')}}" class="btn btn-danger ">Cancelar</a>
                    </form>
                </div>
            </div>
            <br>
        </div>
    </body>
</html>