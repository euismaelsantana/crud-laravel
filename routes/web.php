<?php

use App\Http\Controllers\Funcionario;
use Illuminate\Support\Facades\Route;

Route::get('/', [Funcionario::class, 'listar']);
Route::get('/cadastro', [Funcionario::class, 'novo']);
Route::get('/funcionario/{id}', [Funcionario::class, 'editar']);
Route::post('/funcionario', [Funcionario::class, 'criar']);
Route::put('/funcionario/{id}', [Funcionario::class, 'alterar']);
Route::delete('/funcionario/{id}', [Funcionario::class, 'excluir']);
